#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <memory>
#include <iostream>
#include <cstring>
#include <pcap.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>

using namespace std;

class Controller
{
public:
    Controller();
    ~Controller();

    void Start();
    void Stop();
private:
    pcap_t *handle; /* Session handle */
    int packets;

    void StartPcap();
    static void packet_handler(u_char *param,
                        const struct pcap_pkthdr *header,
                        const u_char *pkt_data);

    int get_link_header_len();
    void parse(u_char *buff, int len);
    void parseEthernet(u_char *buff, int len);

    void dump(char *title, u_char *buff, int len);
    void printlin(char *buff, int offs, int len, int i_tambuff);
};

#endif