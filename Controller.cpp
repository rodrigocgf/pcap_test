#include "Controller.h"

Controller::Controller()
{
    cout << "[Controller::Controller]" << endl;
    packets = 0;
}

Controller::~Controller()
{
    cout << "[Controller::~Controller]" << endl;
}

void Controller::Start()
{
    cout << "[Controller::Start]" << endl;
    StartPcap();
}

void Controller::Stop()
{
    cout << "[Controller::Stop]" << endl;
}

void Controller::StartPcap()
{
    
    char dev[] = "eth0";            /* Device to sniff on */
    char errbuf[PCAP_ERRBUF_SIZE]; /* Error string */
    struct bpf_program fp;         /* The compiled filter expression */
    char filter_exp[] = "icmp";    /* The filter expression */
    bpf_u_int32 mask;              /* The netmask of our sniffing device */
    bpf_u_int32 net;               /* The IP of our sniffing device */

    
    if (pcap_lookupnet(dev, &net, &mask, errbuf) == -1)
    {
        fprintf(stderr, "Can't get netmask for device %s\n", dev);
        net = 0;
        mask = 0;
    }
    
    handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == NULL)
    {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
        return;
    }
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1)
    {
        fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
        return;
    }
    if (pcap_setfilter(handle, &fp) == -1)
    {
        fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
        return;
    }

    /* start the capture */
    pcap_loop(handle, 0, &Controller::packet_handler, reinterpret_cast<u_char *>(this));
}

void Controller::packet_handler(u_char *param,
                    const struct pcap_pkthdr *header,
                    const u_char *pkt_data)
{
    struct tm ltime;
    char timestr[16];
    time_t local_tv_sec;
    Controller *pController = reinterpret_cast<Controller *>(param);

    if ((pkt_data != NULL) && (header->len > 0))
    {
        pController->dump("icmp", const_cast<u_char *>(pkt_data), header->len);
        pController->parseEthernet(const_cast<u_char *>(pkt_data), header->len);
        pController->parse(const_cast<u_char *>(pkt_data), header->len);
    }
}

void Controller::parseEthernet(u_char *buff, int len)
{
    int linkhdrlen = get_link_header_len();
    struct ether_header eth_hdr;

    if (!linkhdrlen)
        return;

    if (linkhdrlen == sizeof(struct ether_header))
    {
        memcpy(&eth_hdr.ether_dhost[0], buff, ETHER_ADDR_LEN);
        memcpy(&eth_hdr.ether_shost[0], &buff[ETHER_ADDR_LEN], ETHER_ADDR_LEN);

        dump("ETHERNET DESTINATION MAC ADDRESS:", &eth_hdr.ether_dhost[0], ETHER_ADDR_LEN);
        dump("ETHERNET SOURCE MAC ADDRESS:", &eth_hdr.ether_shost[0], ETHER_ADDR_LEN);
    }
}

void Controller::parse(u_char *buff, int len)
{
    struct ip *iphdr;
    struct icmp *icmphdr;
    struct tcphdr *tcphdr;
    struct udphdr *udphdr;
    char iphdrInfo[256];
    char srcip[256];
    char dstip[256];
    char *packetptr = reinterpret_cast<char *>(buff);

    int linkhdrlen = get_link_header_len();

    if (!linkhdrlen)
        return;

    // Skip the datalink layer header and get the IP header fields.
    packetptr += linkhdrlen;
    iphdr = (struct ip *)packetptr;
    strcpy(srcip, inet_ntoa(iphdr->ip_src));
    strcpy(dstip, inet_ntoa(iphdr->ip_dst));
    sprintf(iphdrInfo, "ID:%d TOS:0x%x, TTL:%d IpLen:%d DgLen:%d",
            ntohs(iphdr->ip_id), iphdr->ip_tos, iphdr->ip_ttl,
            4 * iphdr->ip_hl, ntohs(iphdr->ip_len));

    // Advance to the transport layer header then parse and display
    // the fields based on the type of hearder: tcp, udp or icmp.
    packetptr += 4 * iphdr->ip_hl;
    switch (iphdr->ip_p)
    {
    case IPPROTO_TCP:
        tcphdr = (struct tcphdr *)packetptr;
        printf("TCP  %s:%d -> %s:%d\n", srcip, ntohs(tcphdr->th_sport),
                dstip, ntohs(tcphdr->th_dport));
        printf("%s\n", iphdrInfo);
        printf("%c%c%c%c%c%c Seq: 0x%x Ack: 0x%x Win: 0x%x TcpLen: %d\n",
                (tcphdr->th_flags & TH_URG ? 'U' : '*'),
                (tcphdr->th_flags & TH_ACK ? 'A' : '*'),
                (tcphdr->th_flags & TH_PUSH ? 'P' : '*'),
                (tcphdr->th_flags & TH_RST ? 'R' : '*'),
                (tcphdr->th_flags & TH_SYN ? 'S' : '*'),
                (tcphdr->th_flags & TH_SYN ? 'F' : '*'),
                ntohl(tcphdr->th_seq), ntohl(tcphdr->th_ack),
                ntohs(tcphdr->th_win), 4 * tcphdr->th_off);
        printf("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n\n");
        packets += 1;
        break;

    case IPPROTO_UDP:
        udphdr = (struct udphdr *)packetptr;
        printf("UDP  %s:%d -> %s:%d\n", srcip, ntohs(udphdr->uh_sport),
                dstip, ntohs(udphdr->uh_dport));
        printf("%s\n", iphdrInfo);
        printf("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n\n");
        packets += 1;
        break;

    case IPPROTO_ICMP:
        icmphdr = (struct icmp *)packetptr;
        printf("\n");
        printf("SOURCE IP ADDRESS: %s -> DESTINATION IP ADDRESS: %s\n", srcip, dstip);
        printf("%s\n", iphdrInfo);
        printf("Code:%d ID:%d Sequence:%d\n", icmphdr->icmp_code,
               ntohs(icmphdr->icmp_hun.ih_idseq.icd_id), ntohs(icmphdr->icmp_hun.ih_idseq.icd_seq));
        if (icmphdr->icmp_type == 0)
            printf("===> ECHO\n");
        else if (icmphdr->icmp_type == 8)
            printf("===> ECHO REPLY\n");
            // printf("Type:%d Code:%d ID:%d Seq:%d\n", icmphdr->icmp_type, icmphdr->icmp_code,
            //         ntohs(icmphdr->icmp_hun.ih_idseq.icd_id), ntohs(icmphdr->icmp_hun.ih_idseq.icd_seq));
            printf("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n\n");
        packets += 1;
        break;
    }
}

int Controller::get_link_header_len()
{
    int linktype;
    int linkhdrlen = 0;

    if (handle == NULL)
        return 0;

    // Determine the datalink layer type.
    if ((linktype = pcap_datalink(handle)) == PCAP_ERROR)
    {
        printf("pcap_datalink(): %s\n", pcap_geterr(handle));
        return 0;
    }

    // Set the datalink layer header size.
    switch (linktype)
    {
    case DLT_NULL:
        linkhdrlen = 4;
        break;

    case DLT_EN10MB:
        linkhdrlen = 14;
        break;

    case DLT_SLIP:
    case DLT_PPP:
        linkhdrlen = 24;
        break;

    default:
        printf("Unsupported datalink (%d)\n", linktype);
        linkhdrlen = 0;
    }

    return linkhdrlen;
}

void Controller::dump(char *title, u_char *buff, int len)
{
    char oldbuff[16];
    int nlin = len >> 4;
    int remainder = len & 0x0F;
    int first = 1;
    int i;

    printf("===========================================================================================\n");
    printf("\n");
    printf("\t\t%s\n", title);
    printf("\n");

    for (i = 0; i < nlin; i++)
    {
        if (i != 0 && memcmp(oldbuff, &buff[i << 4], 16) == 0)
        {
            if (first)
            {
                printf("*\n");
                first = 0;
            }
        }
        else
        {
            first = 1;
            printlin(reinterpret_cast<char *>(buff), i << 4, 16, len);
        }
        memcpy(oldbuff, &buff[i << 4], 16);
    }

    i = nlin;
    if (remainder)
        printlin(reinterpret_cast<char *>(buff), i << 4, remainder, len);

    // parse(buff, len);
    printf("===========================================================================================\n");
}

void Controller::printlin(char *buff, int offs, int len, int i_tambuff)
{
    int i, i_aux;
    unsigned char b_val;
    char bformat[7];

    memset(bformat, 0, sizeof(bformat));
    sprintf(bformat, "%04x\t", offs);

    printf(bformat);

    for (i = 0; i < 16; i++)
    {
        i_aux = offs + i;

        if (i_aux < i_tambuff)
        {
            b_val = (0xff & buff[offs + i]);

            if (i < len)
            {
                memset(bformat, 0, sizeof(bformat));
                sprintf(bformat, "%02x ", b_val);
                printf(bformat);
            }
            else
                printf("   ");
        }
        else
        {
            printf("   ");
        }

        if (i == 7)
            printf(" ");
    }

    printf("  ");

    for (i = 0; i < len; i++)
    {
        if (isprint(buff[offs + i]))
            printf("%c", buff[offs + i]);
        else
            printf(".");
    }

    printf("\n");
}
