#include <iostream>
#include "Controller.h"

using namespace std;
static shared_ptr<Controller> controllerPtr;

void WaitExit();

int main(int argc, char **argv)
{

    controllerPtr.reset(new Controller());
    controllerPtr->Start();

    WaitExit();

    controllerPtr->Stop();

    return 0;
}

void WaitExit()
{
    std::string input;

    do
    {
        std::cout << std::endl;
        std::cout << "\t\tDigite 'quit' para sair." << std::endl;
        std::cin >> input;
    } while (input.compare("quit") != 0);
}
