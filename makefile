BOOST_ROOT = /boost_1_81_0
#BOOST_ROOT = /home/rodrigo/MacOsShared/boost_1_62_0

LIB=-L$(BOOST_ROOT)/stage/lib \
	-L$(BOOST_ROOT)/stage/lib/libboost_thread.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_system.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_filesystem.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_timer.a \
	-L$(BOOST_ROOT)/stage/lib/libboost_exception.a \
	-L/usr/local/lib/libsqlite3.a \
	-L/lib/x86_64-linux-gnu/ \
	-lboost_system -lboost_date_time -lboost_thread -lpthread -lboost_filesystem -lsqlite3 -lpcap

INC=-I$(BOOST_ROOT) -I/usr/include -I/usr/include/pcap/

CPPFLAGS=g++ -m64 -Wall -lstdc++  -std=c++1y  -pthread
CFLAGS=gcc -Wall -Wextra $(pcap-config --cflags)

all: pcap


pcap : main.o Controller.o
	$(CPPFLAGS) -o pcap main.o Controller.o $(LIB) $(INC)

main.o : main.cpp
	$(CPPFLAGS) -g -c main.cpp $(LIB) $(INC)

Controller.o : Controller.cpp
	$(CPPFLAGS) -g -c Controller.cpp $(LIB) $(INC)

clean:
		rm -rf *.so
		rm -rf *.o
		rm pcap